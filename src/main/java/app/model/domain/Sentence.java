package app.model.domain;

import java.util.Arrays;

public class Sentence {

  private StringBuilder sentence;
  private String[] words;

  public Sentence(StringBuilder sentence) {
    this.sentence = sentence;
    getWordsFromSentence();
  }

  public StringBuilder getSentence() {
    return sentence;
  }

  public String[] getWords() { return words; }

  public void setWords(String[] words) {
    this.words = words;
  }

  private void getWordsFromSentence() {
    words = sentence.toString().split("[( )(, )(. )(! )(? )]");
    words = Arrays.stream(words)
        .map(String::toLowerCase)
        .toArray(String[]::new);
  }

  @Override
  public String toString() {
    return sentence.toString();
  }

}
