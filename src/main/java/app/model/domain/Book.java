package app.model.domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Book {

  private final Logger logger = LogManager.getLogger(Book.class);
  private String text;
  private List<Sentence> sentences;

  public Book(String bookPath) {
    getBookTextFromFile(bookPath);
    sentences = new ArrayList<>();
    getSentencesFromBook();
  }

  public String getText() {
    return text;
  }

  public List<Sentence> getSentences() {
    return sentences;
  }

  private void getSentencesFromBook() {
    if (text == null) {
      logger.error("Book not found");
      throw new NullPointerException("Book not found");
    }
    Pattern pattern = Pattern.compile("[\\s\\w][\\w\\s';,:\\-]+[.!?]");
    Matcher matcher = pattern.matcher(text);
    StringBuilder sentence = new StringBuilder();
    while(matcher.find()) {
      sentence.append(matcher.group());
      sentences.add(new Sentence(sentence));
      sentence = new StringBuilder();
    }
  }

  private void getBookTextFromFile(String path) {
    StringBuilder textBuilder = new StringBuilder();
    try {
      Scanner scn = new Scanner(new File(path));
      while (scn.hasNextLine()) {
        textBuilder.append(scn.nextLine());
      }
    } catch (FileNotFoundException e) {
      logger.error(e);
      System.err.println(e);
    }
    this.text = textBuilder.toString();
  }

  private StringBuilder[] stringsToStringBuilders(String[] strings) {
    StringBuilder[] stringBuilders = new StringBuilder[strings.length];
    initStringBuilders(stringBuilders);
    for (int i = 0; i < strings.length; i++) {
      stringBuilders[i].append(strings[i]);
    }
    return stringBuilders;
  }

  private void initStringBuilders(StringBuilder[] stringBuilders) {
    for (int i = 0; i < stringBuilders.length; i++) {
      stringBuilders[i] = new StringBuilder();
    }
  }
}
