package app.model.service.task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import app.model.domain.Book;
import app.model.domain.Sentence;

public class Task13 implements TaskSolvable {

  private final int NECESSARY_PARAMS_NUM = 2;
  private String keyLetter;

  Comparator<String> letterFrequencyComparator = Comparator
      .comparing(this::getLetterFrequency);

  @Override
  public List<String> solve(Object... params) {
    if (params.length != NECESSARY_PARAMS_NUM) {
      throw new IllegalArgumentException("Book must be 1st argument. "
          + "Key letter must be 2nd.");
    }
    List<Sentence> sentences = new ArrayList<>(((Book) params[0]).getSentences());
    keyLetter = (String) params[1];
    return sentences.stream()
        .flatMap(s -> Arrays.stream(s.getWords()))
        .sorted(letterFrequencyComparator.reversed())
        .collect(Collectors.toList());
  }

  private int getLetterFrequency(String word) {
    int frequency = 0;
    for (int i = 0; i < word.length(); i++) {
      frequency += Character.toString(word.charAt(i)).equalsIgnoreCase(keyLetter) ? 1 : 0;
    }
    return frequency;
  }
}
