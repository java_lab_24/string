package app.model.service.task;

@FunctionalInterface
public interface TaskSolvable {

  Object solve(Object... params);
}