package app.model.service.task;

import app.Constant;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import app.model.domain.Book;
import app.model.domain.Sentence;

public class Task8 implements TaskSolvable {

  List<Sentence> sentences;
  Pattern pattern;
  Matcher matcher;
  List<String> words;
  List<String[]> sortedSentences;
  Comparator<String> wordFirstConsonantLetterComparator = Comparator
      .comparing(this::getFirstConsonantLetter);

  public Task8() {
    pattern = Pattern.compile("\\b" + Constant.VOWEL_LETTERS_PATTER + "\\w+\\b");
    words = new LinkedList<>();
    sortedSentences = new LinkedList<>();
  }

  @Override
  public List<String[]> solve(Object... params) {
    if (params[0] == null) {
      throw new IllegalArgumentException("Book not found. Book must be first argument");
    }
    sentences = ((Book) params[0]).getSentences();
    for (int i = 0; i < sentences.size(); i++) {
      matcher = pattern.matcher(arrayToString(sentences.get(i).getWords()));
      while (matcher.find()) {
        words.add(matcher.group());
      }
      sortedSentences.add(sortByFirstConsonantLetter(words));
      words.clear();
      matcher.reset();
    }
    return sortedSentences;
  }

  private String[] sortByFirstConsonantLetter(List<String> words) {
    return words.stream()
        .sorted(wordFirstConsonantLetterComparator)
        .toArray(String[]::new);
  }

  private String getFirstConsonantLetter(String word) {
    Pattern pattern = Pattern.compile(Constant.CONSONANTS_LETTERS_PATTER);
    Matcher matcher = pattern.matcher(word);
    if (matcher.find()) {
      return matcher.group();
    }
    return "z";
  }

  private String arrayToString(String[] array) {
    StringBuilder stringBuilder = new StringBuilder();
    for (String s : array) {
      stringBuilder.append(s + " ");
    }
    return stringBuilder.toString();
  }

}
