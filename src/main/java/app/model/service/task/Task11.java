package app.model.service.task;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import app.model.domain.Book;
import app.model.domain.Sentence;

public class Task11 implements TaskSolvable {

  private final int NECESSARY_PARAMS_NUM = 3;
  private String startSymbol;
  private String endSymbol;
  private Pattern pattern;
  private Matcher matcher;

  @Override
  public List<Sentence> solve(Object... params) {
    if (params.length != NECESSARY_PARAMS_NUM) {
      throw new IllegalArgumentException("Book must be 1st argument. "
          + "Start symbol must be 2nd. End symbol must be 3rd.");
    }
    startSymbol = (String) params[1];
    endSymbol = (String) params[2];
    return ((Book) params[0]).getSentences().stream()
        .peek(this::removeSubString)
        .collect(Collectors.toList());
  }

  private void removeSubString(Sentence s) {
    pattern = Pattern.compile(startSymbol);
    matcher = pattern.matcher(s.getSentence());
    int start;
    if (matcher.find()) {
      start = matcher.start();
    } else {
      return;
    }
    matcher.reset();
    pattern = Pattern.compile(endSymbol);
    matcher = pattern.matcher(s.getSentence());
    int end = 0;
    int findStartPoint = start;
    while (matcher.find(findStartPoint) && findStartPoint < s.getSentence().length()) {
      end = matcher.start();
      findStartPoint = end;
      findStartPoint++;
    }
    if (end == 0) {
      return;
    }
    s.getSentence().delete(start, end);
  }

}
