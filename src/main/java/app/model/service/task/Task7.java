package app.model.service.task;

import app.Constant;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import app.model.domain.Book;
import app.model.domain.Sentence;

public class Task7 implements TaskSolvable {

  @Override
  public Map<Sentence, Integer> solve(Object... params) {
    if (params[0] == null) {
      throw new IllegalArgumentException("Book not found. Book must be first argument");
    }
    List<Sentence> sentences = ((Book)params[0]).getSentences();
    Map<Sentence, Integer> sentenceFloatMap = new LinkedHashMap<>();
    Sentence sentence;
    int vowelLettersNumber;
    int consonantsLettersNumber;
    for (int i = 0; i < sentences.size(); i++) {
      sentence = sentences.get(i);
      vowelLettersNumber = count(sentence, Constant.VOWEL_LETTERS_PATTER);
      consonantsLettersNumber = count(sentence, Constant.CONSONANTS_LETTERS_PATTER);
      sentenceFloatMap.put(sentence,
          Math.round(((float)vowelLettersNumber / (float) consonantsLettersNumber) *100));
    }
    return sentenceFloatMap;
  }

  private int count(Sentence s, String patternString) {
    Pattern pattern = Pattern.compile(patternString);
    Matcher matcher = pattern.matcher(s.getSentence());
    int counter = 0;
    while (matcher.find()) {
      counter++;
    }
    return counter;
  }

}
