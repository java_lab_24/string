package app.model.service.task;

import app.Constant;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import app.model.domain.Book;
import app.model.domain.Sentence;

public class Task12 implements TaskSolvable {

  private final int NECESSARY_PARAMS_NUM = 2;
  private Pattern pattern;
  private Matcher matcher;
  private int wordLength;

  @Override
  public List<Sentence> solve(Object... params) {
    if (params.length != NECESSARY_PARAMS_NUM) {
      throw new IllegalArgumentException("Book must be 1st argument. "
          + "Word length must be 2nd.");
    }
    List<Sentence> result = new ArrayList<>(((Book) params[0]).getSentences());
    wordLength = (int) params[1];
    pattern = Pattern.compile("\\b" + Constant.CONSONANTS_LETTERS_PATTER + "\\w+\\b");
    for (int i = 0; i < result.size(); i++) {
      matcher = pattern.matcher(result.get(i).getSentence());
      while (matcher.find()) {
        if (matcher.group().length() == wordLength) {
          result.get(i).getSentence().delete(matcher.start(), matcher.end());
          matcher.reset();
        }
      }
    }
    return result;
  }
}
