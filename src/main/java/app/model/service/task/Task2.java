package app.model.service.task;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import app.model.domain.Book;
import app.model.domain.Sentence;

public class Task2 implements TaskSolvable {

  @Override
  public List<Sentence> solve(Object... params) {
    if (params[0] == null) {
      throw new IllegalArgumentException("Book not found. Book must be first argument");
    }
    return ((Book)params[0]).getSentences().stream()
        .sorted(Comparator.comparingInt(s -> s.getWords().length))
        .collect(Collectors.toList());
  }
}
