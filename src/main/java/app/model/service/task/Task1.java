package app.model.service.task;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import app.model.domain.Book;
import app.model.domain.Sentence;

public class Task1 implements TaskSolvable {

  @Override
  public Integer solve(Object... params) {
    if (params[0] == null) {
      throw new IllegalArgumentException("Book not found. Book must be first argument");
    }
    return getWordsRepeatedSentencesNumber(((Book)params[0]).getSentences());
  }

  private int getWordsRepeatedSentencesNumber(List<Sentence> sentences) {
    int counter = 0;
    for (int i = 0; i < sentences.size(); i++) {
      if (Arrays.stream(sentences.get(i).getWords())
          .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
          .entrySet().stream()
          .anyMatch(e -> e.getValue() >= 2)) {
        counter++;
      }
    }
    return counter;
  }
}



