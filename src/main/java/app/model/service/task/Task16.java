package app.model.service.task;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import app.model.domain.Book;
import app.model.domain.Sentence;

public class Task16 implements TaskSolvable{

  private final int NECESSARY_PARAMS_NUM = 3;
  private int wordLength;
  private String subString;
  private Pattern pattern;
  private Matcher matcher;

  @Override
  public List<Sentence> solve(Object... params) {
    if (params.length != NECESSARY_PARAMS_NUM) {
      throw new IllegalArgumentException("Book must be 1st argument. "
          + "Word length must be 2nd. Sub string must be 3rd.");
    }
    List<Sentence> result = new ArrayList<>(((Book) params[0]).getSentences());
    wordLength = (int) params[1];
    subString = (String) params[2];
    pattern = Pattern.compile(getStringPattern());
    for (int i = 0; i < result.size(); i++) {
      matcher = pattern.matcher(result.get(i).getSentence());
      while (matcher.find()) {
        result.get(i).getSentence().replace(matcher.start(), matcher.end(), subString);
      }
    }
    return result;
  }

  private String getStringPattern() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("\\b");
    for (int i = 0; i < wordLength; i++) {
      stringBuilder.append("\\w");
    }
    stringBuilder.append("\\b");
    return stringBuilder.toString();
  }
}
