package app.model.service.task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import app.model.domain.Book;
import app.model.domain.Sentence;

public class Task5 implements TaskSolvable {

  @Override
  public List<Sentence> solve(Object... params) {
    if (params[0] == null) {
      throw new IllegalArgumentException("Book not found. Book must be first argument");
    }
    List<Sentence> sentences = new ArrayList<>(((Book)params[0]).getSentences());
    Pattern pattern;
    Matcher matcher;
    for (int i = 0; i < sentences.size(); i++) {
      pattern = Pattern.compile("^[AaEeIiOoUu]\\w+");
      matcher = pattern.matcher(sentences.get(i).getSentence());
      if(matcher.find()) {
        sentences.get(i).getSentence().
            replace(0, matcher.group().length(), getLongestWord(sentences.get(i)));
      }
    }
    return sentences;
  }

  private String getLongestWord(Sentence sentence) {
    return Arrays.stream(sentence.getWords())
        .max(Comparator.comparingInt(String::length))
        .orElse(null);
  }

}
