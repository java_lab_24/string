package app.model.service.task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import app.model.domain.Book;
import app.model.domain.Sentence;

public class Task9 implements TaskSolvable {

  private String keyLetter;

  Comparator<String> letterFrequencyComparator = Comparator
      .comparing(this::getLetterFrequency);

  @Override
  public List<String> solve(Object... params) {
    if (params[0] == null) {
      throw new IllegalArgumentException("Book not found. Book must be first argument");
    }
    if (params[1] == null) {
      throw new IllegalArgumentException(
          "Key letter not found. Key letter must be second argument");
    }
    List<Sentence> sentences = new ArrayList<>(((Book) params[0]).getSentences());
    keyLetter = (String) params[1];
    return sentences.stream()
        .flatMap(s -> Arrays.stream(s.getWords()))
        .sorted(letterFrequencyComparator)
        .collect(Collectors.toList());
  }

  private int getLetterFrequency(String word) {
    int frequency = 0;
    for (int i = 0; i < word.length(); i++) {
      frequency += Character.toString(word.charAt(i)).equalsIgnoreCase(keyLetter) ? 1 : 0;
    }
    return frequency;
  }

}
