package app.model.service.task;

import java.util.List;

public class Task10 implements TaskSolvable {

  private final int NECESSARY_PARAMS_NUM = 3;

  @Override
  public List<String> solve(Object... params) {
    if (params.length != NECESSARY_PARAMS_NUM) {
      throw new IllegalArgumentException("Book must be 1st argument. "
          + "Start symbol must be 2nd. End symbol must 3rd argument.");
    }
    return null;
  }

}
