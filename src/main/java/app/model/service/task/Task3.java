package app.model.service.task;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import app.model.domain.Book;
import app.model.domain.Sentence;

public class Task3 implements TaskSolvable {

  @Override
  public String solve(Object... params) {
    if (params[0] == null) {
      throw new IllegalArgumentException("Book not found. Book must be first argument");
    }
    List<Sentence> sentences = ((Book)params[0]).getSentences();
    String[] firstSentenceWords = sentences.get(0).getWords();
    String lowercaseSentence = listToString(sentences);
    Pattern currentWord;
    Matcher matcher;
    for(int i = 0; i < firstSentenceWords.length; i++) {
      currentWord = Pattern.compile("\\b" + firstSentenceWords[i] + "\\b");
      matcher = currentWord.matcher(lowercaseSentence);
      if (!matcher.find()) {
         return firstSentenceWords[i];
      }
    }
    return null;
  }

  private String listToString(List<Sentence> list) {
    StringBuilder stringBuilder = new StringBuilder();
    for(int i = 1; i < list.size(); i++) {
      stringBuilder.append(" " + arrayToString(list.get(i).getWords()));
    }
    return stringBuilder.toString();
  }

  private String arrayToString(String[] array) {
    StringBuilder stringBuilder = new StringBuilder();
    for(int i = 0; i < array.length; i++) {
      stringBuilder.append(" " + array[i]);
    }
    return stringBuilder.toString();
  }
}
