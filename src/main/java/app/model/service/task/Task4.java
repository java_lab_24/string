package app.model.service.task;

import edu.emory.mathcs.backport.java.util.LinkedList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import app.model.domain.Book;
import app.model.domain.Sentence;

public class Task4 implements TaskSolvable {

  @Override
  public List<String> solve(Object... params) {
    if (params[0] == null) {
      throw new IllegalArgumentException("Book not found. Book must be first argument");
    }
    List<Sentence> questionSentences = getQuestionSentences(((Book)params[0]).getSentences());
    Integer wordLength = (Integer)params[1];
    return questionSentences.stream()
        .flatMap(s -> Arrays.stream(s.getWords()))
        .filter(w -> w.length() == wordLength)
        .distinct()
        .collect(Collectors.toList());
  }

  private List<Sentence> getQuestionSentences(List<Sentence> sentences) {
    List<Sentence> questionSentencesList = new LinkedList();
    for (Sentence s : sentences) {
      Pattern pattern = Pattern.compile("[\\s\\w][\\w\\s';,:\\-]+\\?");
      Matcher matcher = pattern.matcher(s.getSentence());
      if (matcher.find()) {
        questionSentencesList.add(s);
      }
    }
    return questionSentencesList;
  }

}
