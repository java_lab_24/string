package app.model.service.task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import app.model.domain.Book;
import app.model.domain.Sentence;

public class Task6 implements TaskSolvable {

  @Override
  public List<String[]> solve(Object... params) {
    if (params[0] == null) {
      throw new IllegalArgumentException("Book not found. Book must be first argument");
    }
    List<Sentence> sentences = new ArrayList<>(((Book)params[0]).getSentences());
    String[] words;
    for (int i = 0; i < sentences.size(); i++) {
      words = sentences.get(i).getWords();
      sentences.get(i).setWords(
          Arrays.stream(words)
            .sorted(Comparator.comparingInt(String::length))
            .toArray(String[]::new));
    }
    return getWordsList(sentences);
  }

  private List<String[]> getWordsList(List<Sentence> sentences) {
    List<String[]> sentencesWords = new LinkedList<>();
    for (int i = 0; i < sentences.size(); i++) {
      sentencesWords.add(sentences.get(i).getWords());
    }
    return sentencesWords;
  }
}
