package app;

public class Constant {

  public static final String BOOK_PATH = "D:\\Me\\epamUniversity\\offlinePart\\task05\\task05_String\\src\\main\\resources\\thinkingInJava.txt";
  public static final String CONSONANTS_LETTERS_PATTER = "[^AaEeIiOoUu.,!?':\\\\s]";
  public static final String VOWEL_LETTERS_PATTER = "[AaEeIiOoUu]";
}
