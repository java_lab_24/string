package app.controller;

import app.Constant;
import app.model.domain.Book;
import app.model.domain.Sentence;
import app.model.service.task.Task1;
import app.model.service.task.Task10;
import app.model.service.task.Task11;
import app.model.service.task.Task12;
import app.model.service.task.Task13;
import app.model.service.task.Task14;
import app.model.service.task.Task15;
import app.model.service.task.Task16;
import app.model.service.task.Task2;
import app.model.service.task.Task3;
import app.model.service.task.Task4;
import app.model.service.task.Task5;
import app.model.service.task.Task6;
import app.model.service.task.Task7;
import app.model.service.task.Task8;
import app.model.service.task.Task9;
import app.model.service.task.TaskSolvable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

public class Controller {

  private Map<String, TaskSolvable> tasks;
  private Book book;

  public Controller() {
    book = new Book(Constant.BOOK_PATH);
    tasks = new HashMap<>();
    tasks.put("1", new Task1());
    tasks.put("2", new Task2());
    tasks.put("3", new Task3());
    tasks.put("4", new Task4());
    tasks.put("5", new Task5());
    tasks.put("6", new Task6());
    tasks.put("7", new Task7());
    tasks.put("8", new Task8());
    tasks.put("9", new Task9());
    tasks.put("10", new Task10());
    tasks.put("11", new Task11());
    tasks.put("12", new Task12());
    tasks.put("13", new Task13());
    tasks.put("14", new Task14());
    tasks.put("15", new Task15());
    tasks.put("16", new Task16());
  }

  public Map buildMenu(ResourceBundle menuText) {
    Map<String, String> menu = new LinkedHashMap<>();
    menu.put("1. " + menuText.getString("option.1"), "1");
    menu.put("2. " + menuText.getString("option.2"), "2");
    menu.put("3. " + menuText.getString("option.3"), "3");
    menu.put("4. " + menuText.getString("option.4"), "4");
    menu.put("5. " + menuText.getString("option.5"), "5");
    menu.put("6. " + menuText.getString("option.6"), "6");
    menu.put("7. " + menuText.getString("option.7"), "7");
    menu.put("8. " + menuText.getString("option.8"), "8");
    menu.put("9. " + menuText.getString("option.9"), "9");
    menu.put("10. " + menuText.getString("option.10"), "10");
    menu.put("11. " + menuText.getString("option.11"), "11");
    menu.put("12. " + menuText.getString("option.12"), "12");
    menu.put("13. " + menuText.getString("option.13"), "13");
    menu.put("14. " + menuText.getString("option.14"), "14");
    menu.put("15. " + menuText.getString("option.15"), "15");
    menu.put("16. " + menuText.getString("option.16"), "16");
    menu.put("q. " + menuText.getString("option.quit"), "q");
    return menu;
  }

  private String sentencesToString(List<Sentence> sentences) {
    StringBuilder stringBuilder = new StringBuilder();
    sentences.forEach(s -> stringBuilder.append(s.getSentence() + "\n"));
    return stringBuilder.toString();
  }

  private String stringsToString(List<String> strings) {
    StringBuilder stringBuilder = new StringBuilder();
    strings.forEach(s -> stringBuilder.append(s + "\n"));
    return stringBuilder.toString();
  }

  private String stringArraysToString(List<String[]> strings) {
    StringBuilder stringBuilder = new StringBuilder();
    for (String[] sa : strings) {
      for (String s : sa) {
        stringBuilder.append(s + " ");
      }
      stringBuilder.append("\n");
    }
    return stringBuilder.toString();
  }

  private String mapToString(Map<Sentence, Integer> map) {
    StringBuilder stringBuilder = new StringBuilder();
    List<Sentence> sentences = book.getSentences();
    for (int i = 0; i < map.size(); i++) {
      stringBuilder.append(sentences.get(i).getSentence()
          + " --- " + map.get(sentences.get(i)) + "%\n");
    }
    return stringBuilder.toString();
  }

  public String solve(String taskNumber) {
    if (!tasks.containsKey(taskNumber)) {
      throw new IllegalArgumentException("Task not found");
    }
    switch (taskNumber) {
      case "1":
        return ((Integer) tasks.get(taskNumber).solve(book)).toString();
      case "2":
        return sentencesToString((List) tasks.get(taskNumber).solve(book));
      case "3":
        return (String) tasks.get(taskNumber).solve(book);
      case "4":
        return stringsToString((List) tasks.get(taskNumber).solve(book, 5));
      case "5":
        return sentencesToString((List) tasks.get(taskNumber).solve(book));
      case "6":
        return stringArraysToString((List) tasks.get(taskNumber).solve(book));
      case "7":
        return mapToString((Map) tasks.get(taskNumber).solve(book));
      case "8":
        return stringArraysToString((List) tasks.get(taskNumber).solve(book));
      case "9":
        return stringsToString((List) tasks.get(taskNumber).solve(book, "b"));
      case "10":
        return stringArraysToString((List) tasks.get(taskNumber).solve(book));
      case "11":
        return sentencesToString((List) tasks.get(taskNumber).solve(book, "a", "b"));
      case "12":
        return sentencesToString((List) tasks.get(taskNumber).solve(book, 5));
      case "13":
        return stringsToString((List) tasks.get(taskNumber).solve(book, "a"));
      case "14":
        return (String) tasks.get(taskNumber).solve(book);
      case "15":
        return (String) tasks.get(taskNumber).solve(book);
      case "16":
        return sentencesToString((List) tasks.get(taskNumber).solve(book, 5, "SUB STRING"));
    }
    return null;
  }
}
