package app.view;

import app.controller.Controller;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainView {

  private static final Logger logger = LogManager.getLogger(MainView.class);
  private static Scanner scn = new Scanner(System.in);

  private Map<String, Locale> languagesMenu;
  private Controller controller;
  private ResourceBundle menuText;
  private Map<String, String> menu;

  public MainView() {
    languagesMenu = new LinkedHashMap<>();
    languagesMenu.put("English", new Locale("en", "US"));
    languagesMenu.put("Українська", new Locale("uk", "UA"));
    languagesMenu.put("Türkçe", new Locale("tr", "TR"));
    controller = new Controller();
  }

  private void printMenu(Map menu) {
    menu.forEach((k, v) -> System.out.println(k));
  }

  public void show() {
    System.out.println("Task05. String");
    String keyMenu;
    do {
      System.out.println("\nLanguages");
      printMenu(languagesMenu);
      System.out.print("Select your language: ");
      keyMenu = scn.nextLine();
    } while (!languagesMenu.containsKey(keyMenu));
    menuText = ResourceBundle
        .getBundle("messages", languagesMenu.get(keyMenu));
    menu = controller.buildMenu(menuText);
    do {
      System.out.println();
      printMenu(menu);
      System.out.print(menuText.getString("option.prompt"));
      keyMenu = scn.nextLine();
      try {
        System.out.println("\n" + controller.solve(keyMenu));
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("q"));
  }

}
